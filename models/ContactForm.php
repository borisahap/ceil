<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $phone;
    public $last_name;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name'], 'required','message' => 'Имя не может быть пустым'],
            [['phone'], 'required','message' => 'Телефон не может быть пустым'],
            ['last_name','safe']
            
        ];
    }
    
}
