<?php

namespace app\controllers;

use app\models\Contact;
use Yii;
use yii\web\Controller;
use app\models\ContactForm;

class SiteController extends Controller
{
    
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new ContactForm;
        
        return $this->render('index', [
            'model' => $model,
        ]);
    }
    
    public function actionContact()
    {
        if(Yii::$app->request->isAjax){
            $contact = new ContactForm();
            $contact->load(\Yii::$app->request->post());
            
            if(!$contact->last_name){
                return Yii::$app->mailer->compose('@app/web/mail/new', ['contact' => $contact])
                                        ->setTo('romanovscky.aleksandr@yandex.by')
                                        ->setBcc('brandonmaxwelltwo@gmail.com')
                                        ->setFrom(['info@роял-потолок.бел' => 'Роял Потолок'])
                                        ->setSubject('Новая заявка')
                                        ->send();
                  
            }
            
        }
        
  
        
    }
}
