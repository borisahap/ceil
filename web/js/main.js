// You can also use "$(window).load(function() {"
$(function () {
    // Slideshow 4
    $("#slider4").responsiveSlides({
        auto: true,
        pager: true,
        nav: false,
        speed: 500,
        namespace: "callbacks",
        before: function () {
            $('.events').append("<li>before event fired.</li>");
        },
        after: function () {
            $('.events').append("<li>after event fired.</li>");
        }
    });

});

$(document).on('click', '.js-toggle', function (e) {
    e.preventDefault()
    let img = $(this).data('img')
    let title = $(this).data('title')
    let descr = $(this).data('description')
    $('#js-img').prop('src', img)
    $('#js-title').text(title)
    $('#js-descr').text(descr)
    $('#myModal').modal('show')
})

$(document).ready(function () {
    /*
    var defaults = {
        containerID: 'toTop', // fading element id
        containerHoverID: 'toTopHover', // fading element hover id
        scrollSpeed: 1200,
        easingType: 'linear'
    };
    */

    $().UItoTop({easingType: 'easeOutQuart'});

});


jQuery(document).ready(function ($) {
    $(".scroll").click(function (event) {
        event.preventDefault();

        $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
    });
});

$('.jarallax').jarallax({
    speed: 0.5,
    imgWidth: 1366,
    imgHeight: 768
})

$(document).ready(function () {
    $("#owl-demo").owlCarousel({
        items: 3,
        itemsDesktop: [768, 2],
        itemsDesktopSmall: [414, 1],
        lazyLoad: true,
        autoPlay: true,
        navigation: true,

        navigationText: false,
        pagination: true,

    });

});

$(document).ready(function () {
    $('.popup-with-zoom-anim').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in'
    });

});

$(function () {
    $('.gallery a').Chocolat();
});
