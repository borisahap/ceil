<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class AppAsset extends AssetBundle
{
    
    public $basePath = '@webroot';
    
    public $baseUrl  = '@web';
    
    public $css      = [
        'css/bootstrap.css',
        'css/style.css',
        'css/font-awesome.css',
        'css/owl.carousel.css',
        'css/chocolat.css',
    ];
    
    public $js       = [
        //'js/jquery-2.1.4.min.js',
        'js/bootstrap.js',
        'js/responsiveslides.min.js',
        'js/move-top.js',
        'js/easing.js',
        'js/jarallax.js',
        //'js/jquery.vide.min.js',
        'js/owl.carousel.js',
        'js/jquery.magnific-popup.js',
        'js/jquery.chocolat.js',
        'js/main.js',
    ];
    
    public $depends  = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        JqueryAsset::class,
    ];
}
