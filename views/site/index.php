<?php

/* @var $this yii\web\View */

use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

?>
<!-- About -->
<div class="content-agileits" id="about">
    <div class="welcome-w3">
        <div class="container">
            <h3 class="w3_tittle">О Нас</h3>
            <div class="welcome-grids">
                <div class="col-md-5 wel-grid1">
                    <div class="port-7 effect-2">
                        <div class="image-box">
                            <img src="images/kids-item1.jpg" alt="Image-2">
                        </div>
                        <div class="text-desc" style="height: auto">
                            <h4>Глянцевые потолки</h4>
                            <h4>Матовые потолки</h4>
                            <h4>Тканевые потолки</h4>
                            <h4>Сатиновые потолки</h4>
                            <h4>Парящие потолки</h4>
                            <h4>Световые линии</h4>
                            <h4>Трековые светильники</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 wel-grid">
                    <h4>Роял потолок</h4>
                    <p>Более чем 10-летний опыт работы в сфере натяжных потолков позволяет специалистам нашей компании с легкостью воплощать различные идеи оформления потолков. Основной
                       спектр наших работ — комплексное предоставление услуг по производству и установке натяжных потолков «под ключ» в квартиру, коттедж, офис, магазин и другие помещения.
                       Наша компания использует материалы только проверенных производителей, высокое качество комплектующих — неотъемлемое условие корпоративной политики компании, благодаря чему
                       полученный результат будет радовать клиента долгие годы.</p>
                    <p>
                        К приятным бонусам нашей фирмы по натяжным потолкам относятся:</p>
                    <ul style="margin-left: 35px">
                        <li>бесплатный вызов замерщика по городу и пригородам;</li>
                        <li>в цену потолка входит доставка и установка;</li>
                        <li>работаем без предоплаты;</li>
                        <li>устанавливаем натяжные потолки в будние дни, выходные и праздники;</li>
                        <li>возможность оформить договор на дому;</li>
                        <li>гарантия качества монтажа.</li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div><!-- //About --><!-- services -->
<div class="services jarallax" id="services">
    <div class="">
        <h3 class="w3_tittle">Услуги</h3>
        <div class="services-w3lsrow agileits-w3layouts d-flex ">
            <div class="col-md-2 col-sm-2 col-xs-6 services-grids">
                <i class="fa fa-gears" aria-hidden="true"></i>
                <h4 style="font-size: 16px">Дизайн</h4>
                <p>По желанию заказчика потолок может быть классического белого цвета, а может изображать звездное небо или кучевые облака. Услуга дизайна подразумевает проектирование натяжного
                   потолка под конкретное помещение, выбор фактуры и цвета, подбор освещение и комбинация нескольких уровней и рисунков.</p>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-6 services-grids">
                <div class="w3agile-servs-img">
                    <i class="fa fa-group" aria-hidden="true"></i>
                    <h4 style="font-size: 16px">Установка натяжного потолка</h4>
                    <p>Процесс установки потолка начинается с замера и завершается креплением декоративной ленты на местах стыков потолка и стены. Монтаж потолка выполняется специалистами компании,
                       прошедшими обучение и обладающими большим опытом установки. Для монтажа используются современные инструменты и материалы высокого качества.</p>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-6 services-grids">
                <div class="w3agile-servs-img">
                    <i class="fa fa-briefcase" aria-hidden="true"></i>
                    <h4 style="font-size: 16px">Звукоизоляция</h4>
                    <p>Изолировать помещение от шума сверху поможет услуга звукоизоляции. Между основным потолком и натяжным прокладывается специальный материал, который поглощает шум. </p>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-6 services-grids">
                <div class="w3agile-servs-img">
                    <i class="fa fa-clone" aria-hidden="true"></i>
                    <h4 style="font-size: 16px">Ремонт натяжного потолка</h4>
                    <p>Ремонт потолка может потребоваться в случае механического повреждения, например, пореза или разрыва. Наши специалисты устранят дефект, последствия которого будут практически
                       незаметны.</p>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-6 services-grids">
                <div class="w3agile-servs-img">
                    <i class="fa fa-tint" aria-hidden="true"></i>
                    <h4 style="font-size: 16px">Слив воды</h4>
                    <p>Слив воды может потребоваться в случае протечки сверху. Процедура включает в себя частичный демонтаж потолка, чистку, просушку и обработку потолка противогрибковым средством и
                       монтаж потока.</p>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-6 services-grids">
                <div class="w3agile-servs-img">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                    <h4 style="font-size: 16px">Замена натяжного потолка (Trade-In)</h4>
                    <p>Вам произвели некачественную установку или просто появилось желание сменить один натяжной потолок на другой? И без особых финансовых потерь?
                       Теперь у Вас появилась такая возможность! Трейд-ин в натяжных потолках стал реальностью.
                       Наша компания открывает новое и уникальное направление – обмен старого натяжного потолка на новый! Это не просто обычная замена натяжного потолка, а возможность выгодно для
                       Вас
                       получить
                       новый потолок в зачет старого.</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div><!-- //services --><!-- News -->
<div class="w3l-news-content">
    <div class="container">
        <h3 class="w3_tittle">Наши последние работы</h3>
        <div class="col-md-6 w3l-news">
            <div class="media response-info">
                <div class="media-left response-text-left">
                    <a href="" data-img="images/2gostin.jpg" data-title="Глянцевый натяжной потолок"
                       data-description="Установлен глянцевый натяжной потолок в гостинной. Засчет глянца визуально расширено помещение. Отличный выбор для небольших помещений" class="js-toggle">
                        <img class="media-object" src="images/2gostin.jpg" alt="">
                    </a>
                </div>
                <div class="media-body response-text-right">
                    <h5>Глянцевый натяжной потолок</h5>
                    <ul>
                        <li><i class="fa fa-calendar" aria-hidden="true"></i>10 февраля 2021</li>
                    </ul>
                    <p></p>
                    <a href="" class="view resw3 js-toggle" data-img="images/2gostin.jpg" data-title="Глянцевый натяжной потолок"
                       data-description="Установлен глянцевый натяжной потолок в гостинной. Засчет глянца визуально расширено помещение. Отличный выбор для небольших помещений">Смотреть</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="media response-info">
                <div class="media-left response-text-left">
                    <a href="#" class="js-toggle" data-img="images/2tkanevie.jpg" data-title="Тканевый натяжной потолок"
                       data-description="В классическом виде такой потолок неотличим от оштукатуренного (разумеется, так идеально ровно побелку вывести невозможно). Гамма исходных оттенков не слишком богата, но после установки перекрашивать тканевый потолок можно до пяти раз, а так же возможно нанесение различных узоров или рисунков на покрытие.">
                        <img class="media-object" src="images/2tkanevie.jpg" alt="">
                    </a>
                </div>
                <div class="media-body response-text-right">
                    <h5>Тканевый натяжной потолок</h5>
                    <ul>
                        <li><i class="fa fa-calendar" aria-hidden="true"></i>19 января 2021</li>
                    </ul>
                    <a href="#" class="view resw3 js-toggle" data-img="images/2tkanevie.jpg" data-title="Тканевый натяжной потолок"
                       data-description="В классическом виде такой потолок неотличим от оштукатуренного (разумеется, так идеально ровно побелку вывести невозможно). Гамма исходных оттенков не слишком богата, но после установки перекрашивать тканевый потолок можно до пяти раз, а так же возможно нанесение различных узоров или рисунков на покрытие.">Смотреть</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="media response-info">
                <div class="media-left response-text-left">
                    <a href="#" class="js-toggle" data-img="images/IMG_9309.jpg" data-title="Тканевый натяжной потолок"
                       data-description="Текстильные натяжные потолки выбирают те, кто является приверженцем экологически чистых материалов. Благодаря особому производству полотен, обработке полимерными пропитками, они отличаются повышенной прочностью. Им присуща декоративность, выразительность.">
                        <img class="media-object" src="images/IMG_9309.jpg" alt="">
                    </a>
                </div>
                <div class="media-body response-text-right">
                    <h5>Тканевый натяжной потолок</h5>
                    <ul>
                        <li><i class="fa fa-calendar" aria-hidden="true"></i>8 января 2021</li>
                    </ul>
                    <a href="#" class="view resw3 js-toggle" data-img="images/IMG_9309.jpg" data-title="Тканевый натяжной потолок"
                       data-description="Текстильные натяжные потолки выбирают те, кто является приверженцем экологически чистых материалов. Благодаря особому производству полотен, обработке полимерными пропитками, они отличаются повышенной прочностью. Им присуща декоративность, выразительность.">Смотреть</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="media response-info">
                <div class="media-left response-text-left">
                    <a href="#" class="js-toggle" data-img="images/viber_image_2021-03-28_07-04-02.jpg" data-title="Парящий потолок">
                        <img class="media-object" height="180px" src="images/viber_image_2021-03-28_07-04-02.jpg" alt="">
                    </a>
                </div>
                <div class="media-body response-text-right">
                    <h5>Парящий потолок</h5>
                    <ul>
                        <li><i class="fa fa-calendar" aria-hidden="true"></i>26 декабря 2020</li>
                    </ul>
                    <a href="#" class="view resw3 js-toggle" data-img="images/IMG_9309.jpg" data-title="Тканевый натяжной потолок">Смотреть</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-6 w3l-news">
            <div class="media response-info">
                <div class="media-left response-text-left">
                    <a href="#" class="js-toggle" data-img="images/2g.jpg" data-title="Глянцевый натяжной потолок">
                        <img class="media-object" src="images/2g.jpg" alt="">
                    </a>
                </div>
                <div class="media-body response-text-right">
                    <h5>Глянцевый натяжной потолок</h5>
                    <ul>
                        <li><i class="fa fa-calendar" aria-hidden="true"></i>8 февраля 2021</li>
                    </ul>
                    <a href="#" class="view resw3 js-toggle" data-img="images/2g.jpg" data-title="Глянцевый натяжной потолок">Смотреть</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="media response-info">
                <div class="media-left response-text-left">
                    <a href="#" class="js-toggle" data-img="images/tkan.jpg" data-title="Тканевый натяжной потолок" data-description="Благодаря устойчивости к низким температурам допустима установка в помещениях неотапливаемых.
Являются безопасными для здоровья.
Создают в помещении дополнительную защиту от проникновения посторонних звуков, сохраняют тепло.">
                        <img class="media-object" src="images/tkan.jpg" alt="">
                    </a>
                </div>
                <div class="media-body response-text-right">
                    <h5>Тканевый натяжной потолок</h5>
                    <ul>
                        <li><i class="fa fa-calendar" aria-hidden="true"></i>10 января 2021</li>
                    </ul>
                    <a href="#" class="view resw3 js-toggle" data-img="images/tkan.jpg" data-title="Тканевый натяжной потолок" data-description="Благодаря устойчивости к низким температурам допустима установка в помещениях неотапливаемых.
Являются безопасными для здоровья.
Создают в помещении дополнительную защиту от проникновения посторонних звуков, сохраняют тепло.">Смотреть</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="media response-info">
                <div class="media-left response-text-left">
                    <a href="#" class="js-toggle" data-img="images/mat.jpg" data-title="Матовый натяжной потолок">
                        <img class="media-object" src="images/mat.jpg" alt="">
                    </a>
                </div>
                <div class="media-body response-text-right">
                    <h5>Матовый натяжной потолок</h5>
                    <ul>
                        <li><i class="fa fa-calendar" aria-hidden="true"></i>5 января 2021</li>
                    </ul>
                    <a href="#" class="view resw3 js-toggle" data-img="images/mat.jpg" data-title="Матовый натяжной потолок">Смотреть</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="media response-info">
                <div class="media-left response-text-left">
                    <a href="#" class="js-toggle" data-img="images/viber_image_2021-03-28_07-03-28.jpg" data-title="Трековые светильники"
                       data-description="Заменяют целую осветительную группу. В небольших и средних помещениях можно вполне обойтись одной шинной системой вместо центральной лампы и группы точечных светильников.">
                        <img class="media-object" src="images/viber_image_2021-03-28_07-03-28.jpg" alt="">
                    </a>
                </div>
                <div class="media-body response-text-right">
                    <h5>Трековые светильники</h5>
                    <ul>
                        <li><i class="fa fa-calendar" aria-hidden="true"></i>29 декабкря 2020</li>
                    </ul>
                    <a href="#" class="view resw3 js-toggle" data-img="images/viber_image_2021-03-28_07-03-28.jpg" data-title="Трековые светильники"
                       data-description="Заменяют целую осветительную группу. В небольших и средних помещениях можно вполне обойтись одной шинной системой вместо центральной лампы и группы точечных светильников.">Смотреть</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>
    </div>
</div><!-- Modal1 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 id="js-title">Our Top News</h5>
                <img src="images/n1.jpg" id="js-img" alt="blog-image"/>
                <span id="js-descr">&nbsp;</span>
            </div>
        </div>
    </div>
</div>


<!-- //Modal1 --><!-- //News --><!-- pricing -->
<div class="w3l-news-content">
    <div class="pricing-plans center-container" id="prices">
        <div class="container">
            <h3 class="w3_tittle">Наши цены</h3>
            <div class="pricing-grids">
                <div class="pricing-grid1">
                    <div class="price-value">
                        <h3><a href="#">от 20руб/кв.м</a></h3>
                        <h5>Глянцевые потолки</h5>
                    </div>
                    <div class="price-bg">
                        <ul>
                            <li class="whyt"><a href="#">Создает эффект большей освещенности помещения</a></li>
                            <li><a href="#">Экологическая/биологическая чистота</a></li>
                            <li class="whyt"><a href="#">Надежность, прочность и износостойкость</a></li>
                            <li><a href="#">Легкость в уходе</a></li>
                            <li class="whyt"><a href="#">Цена</a></li>
                        </ul>
                        <div class="cart1">
                            <a class="" href="#contact">Заказать</a>
                        </div>
                    </div>
                </div>
                <div class="pricing-grid2">
                    <div class="price-value two">
                        <h3><a href="#">от 20руб/кв.м</a></h3>
                        <h5>Матовые потолки</h5>
                    </div>
                    <div class="price-bg">
                        <ul>
                            <li class="whyt"><a href="#">Полная маскировка любых недостатков</a></li>
                            <li><a href="#">Возможность выравнивания уровня потолка;</a></li>
                            <li class="whyt"><a href="#">Защита от протечек и залитий</a></li>
                            <li><a href="#">Не выделяет запах, токсичные вещества</a></li>
                            <li class="whyt"><a href="#">Долговечность</a></li>
                        </ul>
                        <div class="cart2">
                            <a class="" href="#contact">Заказать</a>
                        </div>
                    </div>
                </div>
                <div class="pricing-grid3">
                    <div class="price-value three">
                        <h3><a href="#">от 45руб/кв.м</a></h3>
                        <h5>Тканевые потолки</h5>
                    </div>
                    <div class="price-bg">
                        <ul>
                            <li class="whyt"><a href="#">Прочность</a></li>
                            <li><a href="#">Термостойкость</a></li>
                            <li class="whyt"><a href="#">Хорошая звукоизоляция</a></li>
                            <li><a href="#">Безопасность</a></li>
                            <li class="whyt"><a href="#">Простота монтажа/демонтажа</a></li>
                        </ul>
                        <div class="cart3">
                            <a class="" href="#contact">Заказать</a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>


<!-- feedback -->
<div class="feedback_dot jarallax">
    <div class=" feedback">
        <div class="container">
            <h3 class="w3_tittle">Отзывы</h3>
            <div class="agileits-feedback-grids">
                <div id="owl-demo" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="icon-w3l">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                        <div class="feedback-info">
                            <div class="feedback-top">
                                <p>Заказали глянцевый потолок в зал. Работой и качеством материала довольны, установку произвели за несколько часов.</p>
                                <div class="feedback-img-info">
                                    <h5>Виктория</h5>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="item">
                        <div class="icon-w3l">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                        <div class="feedback-info">
                            <div class="feedback-top">
                                <p>Специалисты компании помогли определить дизайн потолка, так же разработали план освещения для выбранного дизайна. Очень довольна ценой, качеством и результатом
                                   работы!</p>
                                <div class="feedback-img-info">
                                    <h5>Александр</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="icon-w3l">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                        <div class="feedback-info">
                            <div class="feedback-top">
                                <p>Хорошая компания. Заказывал натяжной потолок на кухню и в спальню. Получилось очень красиво и современно. Большой ассортимент и нормальные цены. Отношение к
                                   клиенту человечное. Буду рекомендовать</p>
                                <div class="feedback-img-info">
                                    <h5>Александр</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="icon-w3l">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                        <div class="feedback-info">
                            <div class="feedback-top">
                                <p>Огромное спасибо мастеру Николаю!к сожалению имя второго мастера не помню. Работу сделали оперативно, аккуратно. Буду рекомендовать друзьям.</p>
                            </div>
                            <div class="feedback-img-info">
                                <h5>Кирилл</h5>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="icon-w3l">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                        <div class="feedback-info">
                            <div class="feedback-top">
                                <p> Большое спасибо! Это уже второй потолок у вас. Мастер Евгений все сделал быстро и качественно. От замера всего два дня и потолок на месте...</p>
                                <div class="feedback-img-info">
                                    <h5>Мария</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="icon-w3l">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                        <div class="feedback-info">
                            <div class="feedback-top">
                                <p>Приятно удивлен выпполнением работ компанией. Вежливое и приятное общение и с менеджером, замерщиком Алексеем и с установщикамми. Толково, все по-делу,
                                   профессионально.</p>
                                <div class="feedback-img-info">
                                    <h5>Станислав</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="icon-w3l">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                        <div class="feedback-info">
                            <div class="feedback-top">
                                <p>Очень довольны натяжными потолками вашей компании. Спасибо мастерам Вячеславу и Андрею. Профессионально. Порекомендую друзьям. Монтаж был 13.09.2017 г..</p>
                                <div class="feedback-img-info">
                                    <h5>Настя</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="icon-w3l">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                        <div class="feedback-info">
                            <div class="feedback-top">
                                <p>Большое спасибо ВАШЕЙ компании, вашим МАСТЕРАМ, МЕНЕДЖЕРАМ! Вы настоящие профессионалы! Долго искали компанию, остановились на вашей, и не ошиблись. </p>
                                <div class="feedback-img-info">
                                    <h5>Алина С.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="icon-w3l">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                        <div class="feedback-info">
                            <div class="feedback-top">
                                <p> Очень довольна работой мастеров. Все быстро, чисто, аккуратно, доброжелательно!!!</p>
                                <div class="feedback-img-info">
                                    <h5>Ирина М.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="icon-w3l">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                        <div class="feedback-info">
                            <div class="feedback-top">
                                <p>Заказывали потолки в две спальни и зал. Очень довольны качеством выполненных работ - от замера до монтажа всё чётко и быстро. Большое спасибо всей компании и
                                   отдельная благодарность мастерам. Договор №00446 от 14.04.2017 г.</p>
                                <div class="feedback-img-info">
                                    <h5>Владимир</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- //feedback --><!-- gallery-->
<div class="gallery" id="gallery">
    <div class="container">
        <h3 class="w3_tittle">Наши работы</h3>
        <div class="gallery-grids">
            <section>
                <ul id="da-thumbs" class="da-thumbs">
                    <li>
                        <a href="images/2gostin.jpg" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/2gostin.jpg" height="272px" alt=""/>
                            <div>
                                <h5>Глянцевый потолок</h5>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="images/2tkanevie.jpg" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/2tkanevie.jpg" height="272px" alt=""/>
                            <div>
                                <h5>Тканевый потолок</h5>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="images/5-1.png" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/5-1.png" height="272px" alt=""/>
                            <div>
                                <h5>Белый глянцевый потолок</h5>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="images/IMG_9309.jpg" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/IMG_9309.jpg" height="272px" alt=""/>
                            <div>
                                <h5>Тканевый потолок</h5>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="images/port-24.jpg" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/port-24.jpg" height="272px" alt=""/>
                            <div>
                                <h5>Матовый бесшовный</h5>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="images/2g.jpg" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/2g.jpg" height="272px" alt=""/>
                            <div>
                                <h5>Глянцевый потолок</h5>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="images/2.png" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/2.png" height="272px" alt=""/>
                            <div>
                                <h5>Матовый потолок</h5>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="images/tkan.jpg" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/tkan.jpg" height="272px" alt=""/>
                            <div>
                                <h5>Тканевый потолок</h5>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="images/mat.jpg" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/mat.jpg" height="272px" alt=""/>
                            <div>
                                <h5>Матовый потолок</h5>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="images/tk5.jpg" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/tk5.jpg" height="272px" alt=""/>
                            <div>
                                <h5>Тканевый потолок</h5>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="images/viber_image_2021-03-28_07-03-28.jpg" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/viber_image_2021-03-28_07-03-28.jpg" height="272px" alt=""/>
                            <div>
                                <h5>Трековые светильники</h5>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="images/viber_image_2021-03-28_07-04-02.jpg" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/viber_image_2021-03-28_07-04-02.jpg" height="272px" alt=""/>
                            <div>
                                <h5>Парящий потолок</h5>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="images/b2ap3_large_natyazhn.jpg" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/b2ap3_large_natyazhn.jpg" height="272px" alt=""/>
                            <div>
                                <h5>Глянцевый многоуровневый потолок</h5>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="images/port-30.jpg" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/port-30.jpg" height="272px" alt=""/>
                            <div>
                                <h5>Матовый потолок со световыми линиями</h5>
                            </div>
                        </a>
                    </li>
                </ul>
            </section>
        </div>
    </div>
</div><!--//gallery--><!-- Contact -->

<div class="contact" id="contact">
    <div class="container">
        <h3 class="w3_tittle">Заказать звонок</h3>
        <?php $form = ActiveForm::begin(['id' => 'js-submit-form']) ?>
        <div class="row">
            <div class="col-md-5">
                <?= $form->field($model, 'name')->textInput([
                    'style'       => 'padding: 1em;
                height:50px;
    font-size: 1em;
    border: 1px solid #000;
    outline: none;
    letter-spacing: 1px;',
                    'placeholder' => 'Имя',
                ])->label(false) ?>
            </div>
            <div class="col-md-5">
                <?= $form->field($model, 'phone')->widget(MaskedInput::class, [
                    'mask'    => '+375 (9{2}) 9{3}-9{2}-9{2}',
                    'options' =>
                        [
                            'style'       => 'padding: 1em;
                height:50px;
    font-size: 1em;
    border: 1px solid #000;
    outline: none;
    letter-spacing: 1px;',
                            'placeholder' => 'Телефон',
        
                        ],
                ])->label(false) ?>
                <?= $form->field($model, 'last_name')->hiddenInput()->label(false) ?>
            </div>
            <div class="col-md-2">
                <button type="submit" style="ont-size: 1.1em;
    outline: none;
    border: none;
    text-transform: uppercase;
    background: #14cdf5;
    padding: .9em 4em;
    letter-spacing: 1px;
    color: #fff;">Заказать
                </button>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<?php $js = <<< JS
$(document).on('submit','#js-submit-form',function() {
  let form = $(this).serialize()
  $.post({
  url:'/site/contact',
  data:form,
  success:function() {
  swal({
title: "Ok!",
text: "Запрос отправлен! В ближайшее время мы с Вами свяжемся",
icon: "success",
button: true,
});
  }
  })
  return false;
})
JS;
$this->registerJs($js) ?>
